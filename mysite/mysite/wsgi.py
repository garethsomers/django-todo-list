"""
WSGI config for mysite project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

####### Additional code begins
import sys

path = '/hsphere/local/home/thatch/dev.thatch.house/django-todo-list/mysite'
if path not in sys.path:
    sys.path.append(path)
sys.path.append('/hsphere/local/home/thatch/venv/lib/python3.6/site-packages')
##### Additional code ends

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")

application = get_wsgi_application()

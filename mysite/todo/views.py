# Imports
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from .models import Task, List
from .forms import TaskForm, ListForm
from .utils import set_response_message, get_request_message


""" 


Index 


"""
# Create your views here.
@login_required
@never_cache
def view_index(request, context = {}):
	""" Shows the index view which lists the lists """

	# Get the data
	context['lists'] = List.objects.order_by('-date_published')

	# Render and return
	template = loader.get_template('todo/index.html')
	return HttpResponse(template.render(context, request))

""" 


List 


"""
@login_required
@never_cache
def view_list(request, list_id, context = {}):
	""" Shows the list view """

	# Incase a message has been passed we need to load it in
	context = get_request_message(request, context)

	# Get the data
	list_object = get_object_or_404(List, pk=list_id)
	task_objects = list_object.task_set.all()

	context['list'] = list_object
	context['tasks'] = task_objects

	# Render and return
	template = loader.get_template('todo/list.html')
	return HttpResponse(template.render(context, request))

@login_required
@never_cache
def view_new_list(request, context = {}):
	""" Create a new task """

	# Incase a message has been passed we need to load it in
	context = get_request_message(request, context)

	# Set these up early so we can use them later
	form = None

	"""
	If the forms been submitted lets validate and try create a new list
	"""
	if request.method == 'POST':
		# Give the POST data to the form object
		form = ListForm(request.POST)
		if form.is_valid():

			# Form has validated, Lets create the object
			new_list = List.objects.create( name=request.POST.get("name"), date_published=timezone.now() )
			
			# Redirect with message
			print("setting list created thing")
			set_response_message(request, "List Created" )
			return redirect('todo:list', list_id = new_list.id )

		else:
			context['message'] = "Error creating List"

	else:
		form = ListForm()

	# Get the data
	context['form'] = form

	# Render and return
	template = loader.get_template('todo/new_list.html')
	return HttpResponse(template.render(context, request))

@login_required
@never_cache
def view_delete_list(request, list_id, context = {}):
	""" Delete a task and then redirect """

	# Incase a message has been passed we need to load it in
	context = get_request_message(request, context)

	list_object = get_object_or_404(List, pk=list_id)
	list_object.delete()

	# TODO make this a proper redirect
	set_response_message(request, "List Deleted" )
	return redirect('todo:index')

""" 


Task 


"""
@login_required
@never_cache
def view_task(request, task_id, context = {}):
	""" Shows the task view """
	
	# Incase a message has been passed we need to load it in
	context = get_request_message(request, context)

	# Set these up early so we can use them later
	task_object = get_object_or_404(Task, pk=task_id)
	form = None

	"""
	If the forms been submitted we're going to validate it and potentially save it
	"""
	if request.method == 'POST':
		# Give the POST data to the form object
		form = TaskForm(request.POST)
		if form.is_valid():
			task_object.name = request.POST.get("name")
			task_object.description = request.POST.get("description")
			task_object.save()

			# Redirect with message
			set_response_message(request, "Task Updated" )
			return redirect('todo:task', task_id = task_object.id )

		else:
			context['message'] = "Error updating Task"
	else:

		# Nothing has been posted, get the form as usual
		form = TaskForm(initial={ 'name': task_object.name, 'description':task_object.description })

	# Get the data
	context['task'] = task_object
	context['form'] = form
	context['list'] = task_object.list

	# Render and return
	template = loader.get_template('todo/task.html')
	return HttpResponse(template.render(context, request))

@login_required
@never_cache
def view_new_task(request, list_id, context = {}):
	""" Create a new task """

	# Incase a message has been passed we need to load it in
	context = get_request_message(request, context)

	# Set these up early so we can use them later
	list = get_object_or_404(List, pk=list_id)
	form = None

	"""
	If the forms been submitted lets validate and try create a new task
	"""
	if request.method == 'POST':
		# Give the POST data to the form object
		form = TaskForm(request.POST)
		if form.is_valid():

			# As the forms validated, lets create the object
			task_object = list.task_set.create(name=request.POST.get("name"), description=request.POST.get("description"), date_published=timezone.now() )
			
			# Redirect with message
			set_response_message(request, "Task Created" )
			return redirect('todo:task', task_id = task_object.id )
		else:
			
			context['message'] = "Error creating Task"

	else:
		form = TaskForm()

	# Get the data
	context['form'] = form
	context['list'] = list

	# Render and return
	template = loader.get_template('todo/new_task.html')
	return HttpResponse(template.render(context, request))

@login_required
@never_cache
def view_delete_task(request, task_id, context = {}):
	""" Delete a task and then redirect """

	task_object = get_object_or_404(Task, pk=task_id)
	list = List.objects.get(pk=task_object.list.id)
	task_object.delete()

	# Redirect with message
	set_response_message(request, "Task Deleted" )
	return redirect('todo:list', list_id = list.id )
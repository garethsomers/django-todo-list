from django.db import models

# List
class List(models.Model):
	name = models.CharField(max_length=200)
	date_published = models.DateTimeField('date published')

	# Override to string value
	def __str__(self):
		return self.name

	def was_published_recently(self):
		return self.date_published >= timezone.now() - datetime.timedelta(days=1)

# Task
class Task(models.Model):
	name = models.CharField(max_length=200)
	description = models.CharField(max_length=200)
	date_published = models.DateTimeField('date published')
	list = models.ForeignKey(List, on_delete=models.CASCADE)

	# Override to string value
	def __str__(self):
		return self.name

	def was_published_recently(self):
		return self.date_published >= timezone.now() - datetime.timedelta(days=1)
from django import forms

class TaskForm(forms.Form):
    name = forms.CharField(label='Name', max_length=200, widget=forms.TextInput(attrs={'class' : 'input'}))
    description = forms.CharField(label='Description', max_length=200, widget=forms.Textarea(attrs={'class' : 'textarea'}))

class ListForm(forms.Form):
    name = forms.CharField(label='Name', max_length=200, widget=forms.TextInput(attrs={'class' : 'input'}))
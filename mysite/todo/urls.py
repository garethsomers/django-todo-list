from django.conf.urls import url
from . import views
from django.contrib import admin
from django.contrib.auth import views as auth_views
# Set the app name / namespace?
app_name = 'todo'

# Define this apps urls
urlpatterns = [
	# ex: /todo/
    url(r'^$', views.view_index, name='index'),
    # ex: /todo/list/5
    url(r'^new-list/$', views.view_new_list, name='new-list'),
    # ex: /todo/new-list
    url(r'^list/(?P<list_id>[0-9]+)/$', views.view_list, name='list'),
    # ex: /todo/delete-list/5
    url(r'^delete-list/(?P<list_id>[0-9]+)/$', views.view_delete_list, name='delete-list'),
    # ex: /todo/task/5
    url(r'^task/(?P<task_id>[0-9]+)/$', views.view_task, name='task'),
    # ex: /todo/new-task
    url(r'^new-task/(?P<list_id>[0-9]+)/$', views.view_new_task, name='new-task'),
    # ex: /todo/delete-task/5
    url(r'^delete-task/(?P<task_id>[0-9]+)/$', views.view_delete_task, name='delete-task'),
    # ex: /todo/login
    url(r'^login/$', auth_views.login, name='login'),
    # ex: /todo/logout
    url(r'^logout/$', auth_views.logout, name='logout'),
]
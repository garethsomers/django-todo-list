from django.contrib import messages
from django.contrib.messages import get_messages

def set_response_message(request, message):
	request.session['message'] = message
	messages.add_message(request, messages.INFO, message)

def get_request_message(request, context):
	context["message"] = get_messages(request)
	return context